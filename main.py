# This is a sample Python script.
import tkinter
import tkinter.messagebox  # 这个是消息框，对话框的关键
import pyperclip as copy
# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


def showMessage(message):
    tkinter.messagebox.showinfo('提示', '' + message)


isSearchIng = False
sumLine = 0


def rootInit():
    global num1, read, btn, lb
    # path = "C:\\Users\\BINGO\\PycharmProjects\\pythonProject\\file\\2020-09-29.log";
    # read(path, "emdcDevice")
    # 主界面
    top = tkinter.Tk()
    top.iconbitmap('./findico.ico')
    top.title("日志获取工具")
    top.geometry('1068x681+10+10')
    # 搜索工具栏
    line1 = tkinter.Frame(top)
    line2 = tkinter.Frame(top)
    lab = tkinter.Label(line1, text="请输入文件路径：")
    lab.pack(side=tkinter.LEFT)
    # 路径
    filePathVal = tkinter.StringVar(
        value="C:\\Users\\BINGO\\PycharmProjects\\pythonProject\\file\\2020-09-29.log")  # 第一个数字
    filePathView = tkinter.Entry(line1, textvariable=filePathVal, width=60)
    filePathView.pack(side=tkinter.LEFT)
    # 搜索关键字
    lab2 = tkinter.Label(line1, text="关键字：")
    lab2.pack(side=tkinter.LEFT)
    searchKey = tkinter.StringVar(
        value="")  # 第一个数字
    searchView = tkinter.Entry(line1, textvariable=searchKey)
    searchView.pack(side=tkinter.LEFT)
    # 读取状态
    lab3 = tkinter.Label(line1, text="状态：")
    lab3.pack(side=tkinter.LEFT)

    # 搜索方法处理逻辑
    def search():
        global isSearchIng
        if (isSearchIng == False):
            val = filePathVal.get()
            if 0 == len(val):
                lb.insert(tkinter.END, "地址不能为空!")
                showMessage("地址不能为空！")
                isSearchIng = False
                btn["text"] = "搜索"
            else:
                btn["text"] = "停止"
                isSearchIng = True
                searchKeyVal = searchKey.get()
                lb.delete(0, tkinter.END)  # 删除所有数据
                read(val, searchKeyVal)
        else:
            btn["text"] = "搜索"
            isSearchIng = False

    # 读取指定文件，设置过滤关键字
    def read(filePath, key):

        with open(filePath, 'r', encoding='utf-8') as f2:
            sumLine = len(f2.readlines())
            lab3['text'] = str(sumLine)
            # lines = f2.readlines()
            # for l in lines:
            #     if isSearchIng:
            #         lb.insert(tkinter.END, str(l))
            #         lb.update()
            #     else:
            #         break
            f2.close()
            # showMessage("加载完成")
            # return
        global isSearchIng
        with open(filePath, 'r', encoding='utf-8') as f:
            line = f.readline()
            number = 0;
            while line:
                if isSearchIng:
                    if key in line:
                        # print_hi(line)
                        number = number + 1
                        pro = number / sumLine;
                        lab3['text'] = str(round(pro, 2)) + "%"
                        lb.insert(tkinter.END, str("行" + str(number) + " " + line))
                        lb.update()
                    line = f.readline()
                else:
                    print_hi("停止查找")
                    break
            f.close()
            showMessage("查找完成")

    btn = tkinter.Button(line1, text='搜索', command=search, width=30)
    btn.pack(side=tkinter.RIGHT)
    line3 = tkinter.Frame(line2)
    line3.pack(side=tkinter.TOP, fill=tkinter.BOTH)
    # 显示视图
    lb = tkinter.Listbox(line3, width=150, height=35, selectmode=tkinter.BROWSE)

    # 复制
    def copyMessage(event):
        flag = tkinter.messagebox.askyesno("提示", '是否复杂这一行？')
        if flag:
            text = lb.get(lb.curselection())
            print(text)
            copy.copy(text)

    # 双击
    lb.bind('<Button-3>', copyMessage)
    lb.pack(side=tkinter.LEFT)
    # 垂直滚动条
    scrolly = tkinter.Scrollbar(line3, orient=tkinter.VERTICAL)
    scrolly.pack(side=tkinter.RIGHT, fill=tkinter.Y)
    # 水平滚动条
    scrolly2 = tkinter.Scrollbar(line2, orient=tkinter.HORIZONTAL)
    scrolly2.pack(side=tkinter.BOTTOM, fill=tkinter.X)
    lb['yscrollcommand'] = scrolly.set  # 注意垂直滚动条用yscrollcommand
    lb['xscrollcommand'] = scrolly2.set  # 注意垂直滚动条用xscrollcommand
    scrolly['command'] = lb.yview  # 注意垂直滚动条用yview
    scrolly2['command'] = lb.xview  # 注意垂直滚动条用yview
    line1.pack(fill=tkinter.X)
    line2.pack(fill=tkinter.BOTH)
    top.mainloop()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    rootInit()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
