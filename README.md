# ReadLogFile_Python

#### 介绍

1、使用python读取日志文件
2、使用tkinter编写GUI
3、使用pyperclip复制到剪切板
4、使用pyinstaller打包exe





~~~
#打包命令
pyinstaller -F -w G:\automation\test.py
#修改图标（免杀）
pyinstaller -F -w -i G:\automation\tpian.ico G:\automation\test.py
~~~
~~~
常用pyinstaller 命令：
1. -i 给应用程序添加图标
2. -F 指定打包后只生成一个exe格式的文件
3. -D –onedir 创建一个目录，包含exe文件，但会依赖很多文件（默认选项）
4. -c –console, –nowindowed 使用控制台，无界面(默认)
5. -w –windowed, –noconsole 使用窗口，无控制台
6. -p 添加搜索路径
~~~
